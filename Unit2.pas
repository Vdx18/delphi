{*******************************************************}
{                                                       }
{       Distance matrix generator                       }
{       Copyright (c) 2018 Home Company                 }
{       Created by Valiakhmetov Vadim                   }
{                                                       }
{*******************************************************}

unit Unit2;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.StdCtrls, Data.Win.ADODB,
  Vcl.Grids, Vcl.DBGrids;

type
  TTask3 = class(TForm)
    DataSource: TDataSource;
    DBGrid: TDBGrid;
    ADOConnection: TADOConnection;
    ADOQuery: TADOQuery;
    Connect: TButton;
    Password: TEdit;
    Login: TEdit;
    Database: TEdit;
    Server: TEdit;
    LoadData: TButton;
    ADOQuery2: TADOQuery;
    SaveData: TButton;
    procedure ConnectClick(Sender: TObject);
    procedure LoadDataClick(Sender: TObject);
    procedure SaveDataClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

//const
// ������� ������� N + 1 (������� � 0-�� ����-�)
//  N = 351; // �� ���� ���-�� � SELECT CodeTo FROM test.Distance GROUP BY CodeTo
var
  Task3: TTask3;
// g - �������� ������� ����� �����, d - ������������� ������� � ������ �� I �� J
  g, d: array[0..351, 0..351] of Integer;
  N: Integer; // N ��� ���� ������� �� �� Interview (����� � @@ROWCOUNT)
implementation

{$R *.dfm}

procedure TTask3.ConnectClick(Sender: TObject);
var
  ConnectionStr, QueryStr: WideString;
  QueryStrN: String; // N ��� ���� ������� �� �� Interview (����� � @@ROWCOUNT)
begin
// ���������� ���� ���� ��� �������...
  try
    ADOConnection.Connected  := False;
    ADOQuery.Active          := False;
// ������� ������ ��� �������� � SQL-��������, � ����� ���� ���������
    ConnectionStr :=
      'Provider=SQLOLEDB.1;' +
      'Password='         + Password.Text + ';' +
      'Persist Security Info=True;' +
      'User ID='          + Login.Text + ';' +
      'Initial Catalog='  + Database.Text + ';' +
      'Use Procedure for Prepare=1;' +
      'Auto Translate=True;' +
      'Packet Size=4096;' +
      'Workstation ID='   + Server.Text + ';' +
      'Use Encryption for Data=False;' +
      'Tag with column collation when possible=False';

    QueryStrN := 'SELECT COUNT(CodeUnique.CodeTo) AS N FROM ' +
      '(SELECT CodeTo FROM test.Distance GROUP BY CodeTo) AS CodeUnique';
    QueryStr :=
      'SELECT	test.CodeToNum(CodeFrom) AS I, ' +
		  'test.CodeToNum(CodeTo) AS J, '+
  		'Dist,	CodeFrom, CodeTo ' +
      'FROM	test.Distance ' +
      'ORDER BY I, J';
// �������
      ADOConnection.ConnectionString  := ConnectionStr;
      ADOConnection.DefaultDatabase   := Database.Text; //'Interview';
      ADOConnection.Connected := True;
    try
      ADOQuery.SQL.Text       := QueryStrN;
      ADOQuery.Active         := True;
      N                       := ADOQuery.FieldByName('N').AsInteger;
      N                       := N - 1 // �������� ������ ���-� � 0-�� ��������
    except
      ADOConnection.Connected := False;
      ADOQuery.Active         := False;
      ShowMessage('Error in SQL-query "SELECT N"');
      Exit;
    end;
    ShowMessage ('Total of ' + IntToStr(N) + ' unique ways');
    try
      ADOQuery.SQL.Text       := QueryStr;
      ADOQuery.Active         := True;
    except
      ADOConnection.Connected := False;
      ADOQuery.Active         := False;
      ShowMessage('Error in SQL-query "SELECT	test.CodeToNum(CodeFrom)..."');
      Exit;
    end;
    with DBGrid.Columns do begin
      Items[0].Width      := 50;
      Items[0].FieldName  := 'I';
      Items[1].Width      := 50;
      Items[1].FieldName  := 'J';
      Items[2].Width      := 50;
      Items[2].FieldName  := 'Dist';
      Items[3].Width      := 60;
      Items[3].FieldName  := 'CodeFrom';
      Items[4].Width      := 50;
      Items[4].FieldName  := 'CodeTo';
    end;
  except
    on e :  EDatabaseError do begin
      ADOConnection.Connected  := False;
      ADOQuery.Active          := False;
      ShowMessage('Connection error! (login or password is fail!)');
      Exit;
    end;
  end;
//  ShowMessage(IntToStr(ADOQuery.DataSource.DataSet.RecordCount));
  LoadData.Enabled := True;
end;

procedure TTask3.FormActivate(Sender: TObject);
var
  PCSize: Cardinal;
  PCName: array [0..MAX_COMPUTERNAME_LENGTH] of Char;
begin
// ��� ��
  PCSize := MAX_COMPUTERNAME_LENGTH + 1;
  GetComputerName(PCName, PCSize);
// �������-� ����� ��� ��������
  Server.Text   := PCName;
  Database.Text := 'Interview';
  Login.Text    := 'sa';
  Password.Text := 'Vadim_2018';
  ShowMessage('����� �������������� ���������� ���������� ������� ��� ' +
    '�������/������� �� ������� SQLQuery3.sql');
end;

procedure TTask3.LoadDataClick(Sender: TObject);
var
//  g, d: array[0..351, 0..351] of Integer; - ����� �����������, �.�. ��� �� d �������� ������
// g - �������� ������� ����� �����, d - ������������� ������� � ������ �� I �� J
  I, J, {N,} K: Integer;
// I, J - ������� �������, N - ���-�� ������ (-1 �.�. ������ � 0), K - �������
begin
// N ������� ����������, ���������� � ��. ����������...
//  N := 352 - 1; // �� ���� ���-�� � SELECT CodeTo FROM test.Distance GROUP BY CodeTo
// ��������� ������ ������� �����, �.�. ��� �����
  for I := 0 to N do
    for J := 0 to N do
      g[I, J] := 999999999; // ���� ����� ��������� I, J ��� �����(�� ���������)
//  ShowMessage('The array is filled with 999999999');
// � ������ ������� g ������� ���� �� DBGrid (�.�. �� ���� �� [test].[Distance])
// �������� ����� �� �������, �� ��� �� DBGrid �������� (� �� �������!)
  DBGrid.DataSource.Dataset.First;
  while not DBGrid.DataSource.DataSet.Eof do begin
    I := DBGrid.DataSource.Dataset.FieldByName('I').AsInteger;
    J := DBGrid.DataSource.Dataset.FieldByName('J').AsInteger;
    g[I, J] := DBGrid.DataSource.DataSet.FieldByName('Dist').AsInteger;
    DBGrid.DataSource.DataSet.Next;
  end;
  ShowMessage('The array is filled with Data. Test: g[64,32]=' + IntToStr(g[64,32]) + ', g[246,247]=' + IntToStr(g[246,247]));
// ������������� ������� ���������
  d := g;

  {������� �����-� �� ������ ������� �� ���� �� ���-�� �������� N^3.
� ������� d[0..N�1, 0..N�1] �� I-�� ����-�� ������ ����� �� ������-�
������ � �������-� �� ��, ��� � �������� "���������" � ���� ����� �����-��
������� � ������� ������ ������ I�1 (������� �����-�� � 0). ����� ��� I-��
��������, � ����� �������� ������ �� I+1 -��. ��� ����� ��� ������ ���� ������
�����-�� ����� � �������� "���������" I�1 -�� ������� � ���� ��� ������� ���-�,
�� ��������. ����� ������� N+1 ��������, ����� � ������-� � ���-�� "���������"
������ �����-�� �����, � � ������� d ����� �����.
N ����-� * N ����-� * N ����-�, ����� N^3 ��������}

//��������� g[0..N-1, 0..N-1] - ������ � ������� ����� ���� ����� (�����-�),
//���� g[I][J] = 999999999, �� �����/���� ����� I � J ���}
  for I := 1 to N + 1 do
    for J := 0 to N - 1 do
      for K := 0 to N - 1 do
        if d[J][K] > d[J][I - 1] + d[I - 1][K] then
          d[J][K] := d[J][I - 1] + d[I - 1][K];
  ShowMessage('A matrix with minimal paths is created.');
  SaveData.Enabled := True;
end;

procedure TTask3.SaveDataClick(Sender: TObject);
var
  QueryStr, InsStr: string;
  I, J, QC: Integer; // QC - Query Count ������� �����/������ � ������� ����� INSERT INTO
  // QC ����� ��� �����-�� ��� �����. ������� INSERT INTO, ��� ������ ���� 1000 �����
begin
  try
    QueryStr := 'EXEC test.RegenMinDist';
//    QueryStr := 'drop table test.MinDistance';
    ADOQuery2.SQL.Text := QueryStr;
    if ADOConnection.Connected then ADOQuery2.ExecSQL;
  except
    ShowMessage('Error query in "EXEC test.RegenMinDist!"');
    Exit;
  end;
  try
    ADOQuery2.SQL.Clear;
    InsStr := 'INSERT INTO test.MinDistance(I, J, MinDist) VALUES ' + #13+#10;
    QueryStr := '';
    QC := 0;
    for I := 0 to N do
      for J := 0 to N do begin
        QueryStr := QueryStr + '(' + IntToStr(I) + ', ' + IntToStr(J) + ', ' + IntToStr(d[I, J]);
        QC := QC + 1;
        // ��������� ������, ���� �������� ����� ������� d[I, J] ��� QC >= 900
        if ((I = N) and (J = N)) or (QC >= 900) then begin
          QueryStr := QueryStr + ');';
          QueryStr := InsStr + QueryStr;
          ADOQuery2.SQL.Text := QueryStr;
          // ���������� ������� QC � QueryStr, ����� ������� ����� QueryStr ������
          QC := 0;
          QueryStr := '';
          try
            if ADOConnection.Connected then ADOQuery2.ExecSQL;
          except
            ShowMessage('Error query "INSERT INTO test.MinDistance"! Save to Error.sql!');
            ADOQuery2.SQL.SaveToFile(ExtractFilePath(Application.ExeName) + '\Error.sql');
            Exit;
          end;
        end
        else
          QueryStr := QueryStr + '),';// + #13+#10; //Enter �� ����������
        // ��� �� �� ��������� ���������)
        Application.ProcessMessages;
        Caption := '���� �������� � ���������� ��������, ���������� ' +
          IntToStr(I * (N + 1) + J + 1) + ' ������ �� ' + IntToStr ((N + 1) * (N + 1));
      end;
  except
    ADOConnection.Connected  := False;
    ADOQuery.Active          := False;
    ShowMessage('Error creating SQL-query or other. Disconnect.');
    Exit;
  end;
  ShowMessage('Congratulations! The data loaded in the database!');
end;

end.
